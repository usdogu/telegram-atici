import { useStorage } from "@plasmohq/storage"

function OptionsIndex() {
  const [channelId, setChannelId] = useStorage<string>("channel-id", (_) => "-1001138489342");
  const [botToken, setBotToken] = useStorage<string>("bot-token", "");
  return (
    <div id="container">
      Kanal ID'si: <input onChange={(e) => setChannelId(e.target.value)} value={channelId} />
      <br />
      Bot Token: <input onChange={(e) => setBotToken(e.target.value)} value={botToken} />
    </div>
  )
}

export default OptionsIndex