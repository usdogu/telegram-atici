import { Storage } from "@plasmohq/storage"

async function sendMessage(message: string) {
    const storage = new Storage();
    const botToken = await storage.get("bot-token");
    const channelId = await storage.get("channel-id");
    await fetch(`https://api.telegram.org/bot${botToken}/sendMessage?chat_id=${channelId}&text=${message}`);
}

export default sendMessage