import { useState, useEffect } from "react"
import sendMessage from "./telegram"

function IndexPopup() {
  const [msg, setMsg] = useState("");
  const [url, setUrl] = useState("");
  
  useEffect(() => {
    chrome.tabs.query({
      active: true,
      lastFocusedWindow: true
    }, (tabs) => {
      setUrl(tabs[0].url);
    });
  }, []);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        padding: 16
      }}>
      Url: {url}
      <textarea onChange={(e => { setMsg(e.target.value) })}></textarea>
      <button onClick={() => sendMessage(encodeURIComponent(`${url}\n\n${msg}`)).then(() => { })}>Gönder</button>
    </div>
  )
}

export default IndexPopup
